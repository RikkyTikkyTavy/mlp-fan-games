# mlp-fan-games

This project is dedicated to collecting fan games based on My Little Pony: Friendship is Magic.

# Table of contents
1. [Sources](#sources)
2. [Games](#games)
    1. [Platformers](#platformers)
    2. [Arcades](#arcades)
    3. [Quests](#quests)
    4. [RPGs/JRPGs](#rpgs)
    5. [Multiplayer games](#multiplayer)
    6. [Tabletop games](#tabletop)
    7. [Strategies](#strategies)
    8. [3d / Action games](#action)
    9. [Mobile games](#mobile)
    10. [Rhythm/music games](#music)
    11. [Point-and-click games](#point)
    12. [Fightings](#fightings)
    13. [Horror games](#horror)
    14. [Simulators](#simulators)
    15. [Puzzles](#puzzles)
    16. [Visual Novels](#visual)
    17. [Text games](#text)
    18. [Generators](#generators)
    19. [ROM games](#rom)
    20. [Other games](#other)
    22. [Demo bundles](#bundles)


<a name="sources"></a>
# Sources

Petr Hudeček's Personal Website
https://hudecekpetr.cz

* Ponychan
https://www.ponychan.net/chat/res/517595.html

* MLP Fan Art Wikia
http://mlpfanart.wikia.com/wiki/Fan_games

* Equestria Daily
https://www.equestriadaily.com/search/label/Game

* Equestria Gaming
http://www.equestriagaming.net/

* Everypony
https://everypony.ru/category/games

* My Little GameJam
http://mylittlegamejam.com/

* ОКИ
https://tabun.everypony.ru/blog/OKI/

* Two Weeks Game
https://twoweeks.github.io/games-db/?get=twg&comp=5

* Anon Pone
https://www.anonpone.com/

<a name="sources"></a>
# Games

<a name="arcades"></a>
## Platformers

* Тени Кантерлота
https://yadi.sk/d/ovBTUrk1yKJ6Q

* My Little Pony: Equestria Adventure
http://www.mediafire.com/file/6c3xzxwqxqny867/EA.zip

* Tank's Ghastly Gorge Dash
http://sandbox.yoyogames.com/games/196625-tanks-ghastly-gorge-dash

* Pinkie's Lost Elements
http://sandbox.yoyogames.com/games/202970-the-lost-element

* MLP: Super Pinkie Pie
http://gamejolt.com/games/platformer/mlp-super-pinkie-pie/67716/

* Fluttershy Navigate
http://www.equestriagaming.net/2013/02/flash-games-pinkies-balloon-patrolpony.html

* Equestria Adventure
http://www.equestriagaming.net/2013/09/demo-equestria-adventure.html

* Lyra and Bon Bon Save the World
http://www.equestriagaming.net/2014/01/demo-lyra-and-bon-bon-save-world_68.html

* A My Little Pony Platformer Game
http://www.equestriagaming.net/2014/02/a-my-little-pony-platformer-game_56.html

* Pink Pone Action
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews.html

* Derpy Delivery
http://www.equestriagaming.net/2011/04/derpy-delivery.html

* Innocent Luna Game
http://www.equestriagaming.net/2011/10/review-innocent-luna-game.html

* Future Twilight
http://www.equestriagaming.net/2012/04/reviewfuture-twilight.html
http://g2f.nl/yubj60

* Pony on the Moon
http://www.equestriagaming.net/2012/05/pony-on-moonparasprite-invaders.html
http://ponies-in-space.appspot.com/

* The Adventures of Pinkie Pie
http://www.equestriagaming.net/2012/06/the-adventures-of-pinkie-pie.html

* Ponyvasion
http://www.equestriagaming.net/2012/10/ponyvasion.html

* Ponyvania: Order of Equestria
https://www.equestriadaily.com/2018/07/ponyvania-officially-released-full.html
https://www.alphabetagamer.com/ponyvania-order-of-equestria-beta-download/

* Rise of the Clockwork Stallions
https://www.equestriadaily.com/2018/07/rise-of-clockwork-stallions-to-be.html

* Legacy of the Pony
https://www.equestriadaily.com/2014/10/game-trailer-legacy-of-pony.html

* Fallout Equestria: Remains
https://www.equestriadaily.com/2017/11/fallout-equestria-remains-version-6.html

* The Daring Do Game
https://www.equestriadaily.com/2017/07/first-public-demo-for-daring-do-game.html

* Pinkie in Candyland
https://www.equestriadaily.com/2013/03/game-trailers-rotor-harmony-pcg-pinkie.html

* Raycord Legends (demo)
https://www.equestriadaily.com/2018/02/raycord-legends-releases-piano-riding.html

* Starved for Light (no demo)
https://www.equestriadaily.com/2018/06/starved-for-light-dev-journal-7-one.html

* Temno (demo)
https://www.equestriadaily.com/2012/10/game-trailer-temno-limbo-style-ponies.html

* Pony Platforming Project
http://www.dragon-mango.com/ponygame/

    * Original demo
    http://www.dragon-mango.com/ponygame/ponygame.htm

    * Holiday Special
    http://www.dragon-mango.com/ponygame/xmasdemo.htm

    * Minuette vs. Tardiness
    https://www.equestriadaily.com/2013/04/game-minuette-vs-tardiness.html

    * Minty Fresh Adventure
    http://www.dragon-mango.com/ponygame/colgate.htm

    * Fresh Minty Adventure
    http://www.dragon-mango.com/ponygame/Minty

    * Minty Fresh Adventures 2: Always the bridesmaid
    https://www.equestriadaily.com/2013/12/minty-fresh-adventure-2-always.html
    http://www.dragon-mango.com/ponygame/pre-preview/


* Mystery of the Apple Thief
https://www.equestriadaily.com/2015/03/game-mystery-of-apple-thief.html

* Stop the Bats!
https://www.equestriadaily.com/2014/02/game-stop-bats.html

* Drifter
https://www.equestriadaily.com/2013/08/game-drifter.html
https://www.deviantart.com/sylrepony/art/Drifter-Release-394883549
http://www.equestriagaming.net/2013/08/review-drifter.html

* Fluttershy's Bunny Rescue
https://www.equestriadaily.com/2013/01/game-fluttershys-bunny-rescue-super.html

* Applebald (runner)
https://everypony.info/applebald-chto-mozhno-sdelat-za-odnu-noch

* Lunar Defense Force
https://www.equestriadaily.com/2012/10/game-lunar-defense-force.html

* Pinkie's Perilous Platforms
https://www.equestriadaily.com/2012/08/game-pinkie-pies-perilous-platforms.html

* Angel Bunny - Platforming Game
https://www.equestriadaily.com/2012/06/game-trailer-angel-bunny-platforming.html

* MLP: Revenants of Chaos
https://www.deviantart.com/okamikurainya/art/MLP-Revenants-of-Chaos-Ponyville-Public-release-464574077

* Adventure Ponies
http://arcade.equestriagaming.net/2017/06/adventure-ponies-2.html

* Magic Shards Remake
http://www.equestriagaming.net/2013/08/demo-magic-shards-remake.html

* Another Pony Platformer!
https://www.deviantart.com/infinitydash/art/MLP-FiM-Platforming-Game-Test-204724303

* Japanese pony game
https://www.equestriadaily.com/2014/11/awesome-japanese-pony-game-in-works.html

* Mega Mare X
https://www.equestriadaily.com/2014/07/mega-mare-x-gameplay-demo.html

* Megapony
https://www.equestriadaily.com/2013/11/megapony-final-release-now-available.html

* Order of Twilight
https://www.equestriadaily.com/2012/10/game-trailer-order-of-twilight.html

* Cupcake Dreams
https://www.equestriadaily.com/2011/11/game-cupcake-dreams.html

* Spike's Quest
https://www.equestriadaily.com/2011/07/spikes-quest-game-engine-demo-3.html

* The Mane MLP Flash game
https://www.equestriadaily.com/2011/06/mlp-flash-game-update-4.html

* MLP FiM: Flash Game 2
https://www.deviantart.com/yeaka/art/MLP-FiM-Flash-Game-2-ver-3-202342576

* Dash Off!
https://www.equestriadaily.com/2011/06/dash-off.html

* Collect Cupcakes, Lick Ponies
http://www.equestriagaming.net/2011/05/collect-cupcakes-lick-ponies-2-version.html
https://www.equestriadaily.com/2011/04/game-collect-cupcakes-lick-ponies-2.html

* MLP: Dusk of Aetas
https://www.equestriadaily.com/2012/04/messageannouncement-from-shadowbolt.html

* MLP: Rise of Vicis
http://www.equestriagaming.net/2012/08/mlp-rise-of-vicis-demo.html

* Pipsqueak's Thunder Plunder
https://www.equestriadaily.com/2012/06/games-pipsqueaks-thunder-plunder.html

* Rainbow Dash - Pony Disaster
https://www.equestriadaily.com/2012/10/game-trailers-rainbow-dash-pony.html

* Pinkie Pie Adventure
https://sonicffvii.deviantart.com/art/my-little-pony-fan-game-Pinkie-pie-adventure-V-1-5-278454896

* Pinkie in Candyland
http://www.equestriagaming.net/2013/03/pinkie-in-candyland.html

* Stroll
http://www.equestriagaming.net/2013/01/project-updates-megacomp-1.html
http://www.equestriagaming.net/2012/12/review-stroll_15.html

* Gravity Pony
http://www.equestriagaming.net/2012/11/gravity-pony.html
http://www.mediafire.com/?k92s22rrvfjxr9p

* Pinkie Pie's Big Adventure
http://www.equestriagaming.net/2012/11/pinkie-pies-big-adventure.html

* Colors of Time
http://www.equestriagaming.net/2012/10/colors-of-time.html

* Daring Do vs. the Everfree Forest
http://www.equestriagaming.net/2012/09/daring-do-vs-everfree-forest.html

* Pinkie Pie Platformer
http://www.equestriagaming.net/2012/08/pinkie-pie-platformer-beta_8.html

* My Little Platform Game
http://inkwell-pony.deviantart.com/#/d3ch2sl
http://www.equestriagaming.net/2011/04/my-little-platform-game.html

* Neverfree
http://www.equestriagaming.net/2014/02/neverfree_2.html

* Daring Do 8-bit Demo
http://mylittlegamedev.com/forums/showthread.php?tid=1137&pid=6363#pid6363

* Ponyventure: The Rise of Evil
http://www.equestriagaming.net/2014/01/ponyventure-demo_8.html

* Corruption
http://www.equestriagaming.net/2012/10/corruption.html

* Luna: Zero Mission
http://www.mediafire.com/?9ttd1cj95d1city


<a name="arcades"></a>
## Arcades

* Muffin
http://www.equestriagaming.net/2012/10/game-maker-compilation-post-2.html
http://www.mediafire.com/?c4ocpcu9q2cqqjn

* Precure Magica: Deconstruction of Discord
http://biancagames.com/PrecureMagica.zip

* Square Ponies
http://zztfox.deviantart.com/art/Square-Ponies-397328752

* Taking Wing
http://www.deviantart.com/art/Taking-Wing-397874204

* Pegasus Attack
http://www.equestriagaming.net/2012/09/game-maker-compilation-post-1.html

* Pinkie Pie Population Perforation
http://www.equestriagaming.net/2012/11/pinkie-pie-population-perforation.html
http://sparklepeep.deviantart.com/art/Pinkie-Pie-Population-Perforation-Project-338328532

* Ye Olde Time Foal Slapping Simulator
https://www.deviantart.com/sparklepeep/art/Ye-Olde-Time-Foal-Slapping-Simulator-339745841

* Dancing Pony Gorgeous Flames
https://www.equestriadaily.com/2011/03/game-dancing-pony-gorgeous-flames.html
http://www.equestriagaming.net/2011/04/dancing-pony-gorgeous-flames-insomnia.html

* Rainbow Dash's Flight Training
http://www.equestriagaming.net/2011/05/rainbow-dashs-flight-training.html

* Holiday in Ponyville
http://www.equestriagaming.net/2011/06/holiday-in-ponyville.html

* Pony Wings
http://www.equestriagaming.net/2011/10/pony-wings.html

* Cloud Dodger
http://www.equestriagaming.net/2012/04/cloud-dodger.html
http://www.mediafire.com/?f8f37927u79iuav

* Go Fast
http://www.equestriagaming.net/2012/05/go-fast.html

* Purple Tinker's Quack Quest
http://www.equestriagaming.net/2012/07/purpletinkers-quack-quest.html

* Rainbow Wake: the Game
http://www.equestriagaming.net/2012/08/review-rainbow-wake-game.html

* Rainbow Dash Shooter
http://arcade.equestriagaming.net/2012/09/rainbow-dash-shooter-project.html
http://www.equestriagaming.net/2012/09/review-rainbow-dash-shooter-project.html

* Super Derpy: Muffin Attack
http://www.equestriagaming.net/2012/09/super-derpy-muffin-attack-preview.html

* Sunset Defender
http://www.equestriagaming.net/2012/10/sunset-defender.html

* Pony Joust
http://www.equestriagaming.net/2012/12/pony-joust.html

* Pony Blaster in Space 3000
http://www.equestriagaming.net/2013/01/pony-blaster-in-space-3000.html

* Season 3 Minigame compilation
http://www.equestriagaming.net/2013/02/the-sparklepeep-season-3-minigame.html

* QuickSpell
http://www.equestriagaming.net/2013/07/game-quickspell.html

* Defenders of Equestria
http://www.equestriagaming.net/2013/11/defenders-of-equestria-test-demo_11.html

* The Solar Empire strikes back
https://nitrouspony.deviantart.com/art/The-Solar-Empire-Strikes-Back-294624290

* Super Dash Game
https://www.equestriadaily.com/2011/06/super-dash-game.html

* My Little Laser
https://www.equestriadaily.com/2011/11/game-my-little-laser.html

* My Little Cannon
https://www.equestriadaily.com/2011/11/game-my-little-cannon.html

* Scootaloo's Scooter Scooting
https://www.equestriadaily.com/2012/01/game-scootaloos-scooter-scooting.html

* My Little Hand Glider
https://www.equestriadaily.com/2012/01/game-my-little-hang-glider.html

* Go Fast
https://www.equestriadaily.com/2012/02/game-go-fast.html

* Apple Family Cider Attack
https://www.equestriadaily.com/2012/02/game-apple-family-cider-attack.html
http://www.equestriagaming.net/2012/02/reviewcider-attack.html

* AJ's track
http://www.equestriagaming.net/2017/01/gem-hunting-bubbles-road-trips-and.html

* RD Flying
http://www.equestriagaming.net/2017/01/gem-hunting-bubbles-road-trips-and.html
http://empalu.deviantart.com/art/Rd-flying-609955880

* Bubbles
http://www.equestriagaming.net/2017/01/gem-hunting-bubbles-road-trips-and.html
https://www.equestriadaily.com/2014/11/game-bubbles.html

* Cutie Mark Unmagic
https://www.equestriadaily.com/2015/02/game-cutie-mark-unmagic.html

* Last Dash
https://www.equestriadaily.com/2015/11/game-last-dash.html

* AppleJackSoloGame
https://everypony.ru/applejacksologame

* Pink Marmalade
https://www.equestriadaily.com/2018/01/geometry-wars-style-pink-marmalade-pony.html

* Alicorn Princess Blast
https://www.equestriadaily.com/2016/09/alicorn-princess-blast-team-releases.html

* Robin Steele the Waifu Thief 
https://www.equestriadaily.com/2014/07/game-robin-steele-waifu-thief.html

* Choke on My Woona
https://www.equestriadaily.com/2013/12/game-choke-on-my-woona-release-trailer.html

* Elemental Reaction 
https://www.equestriadaily.com/2013/11/game-elemental-reaction.html

* Vine Slicer
https://www.equestriadaily.com/2013/11/game-vine-slicer.html

* Gem Hunt
https://www.equestriadaily.com/2012/06/game-gem-hunt.html

* Spike Toss
https://www.equestriadaily.com/2012/10/game-spike-toss.html

* 1000 Years Ago
https://www.equestriadaily.com/2012/06/game-1000-years-ago.html

* Crystal Crunch Story
https://www.equestriadaily.com/2014/09/game-crystal-crunch-story.html

* Twilight Wing 
https://www.equestriadaily.com/2013/07/game-twilight-wing.html

* Protect the pies (official)
https://www.equestriadaily.com/2013/07/new-hubworld-game-protect-pies.html

* Rarity in Donkey Kong Country
https://www.equestriadaily.com/2013/07/rarity-invades-donkey-kong-country.html

* Parasprite Invaders
https://www.youtube.com/watch?v=Ne4rMhaau1Q

* Pinkie's Balloon Trip 
https://www.equestriadaily.com/2014/03/game-pinkies-balloon-trip.html

* Pinkie's balloon patrol
https://www.equestriadaily.com/2013/02/games-pinkies-balloon-patrol-temno_24.html

* Zapple Hill
https://www.equestriadaily.com/2013/09/game-zapple-hill.html

* Ponysweeper
https://www.equestriadaily.com/2013/08/game-ponysweeper.html

* PinkieSweeper
https://sylrepony.deviantart.com/art/Pinkie-Sweeper-v1-264981055

* Power Ponies Go (official)
https://www.equestriadaily.com/2014/07/power-ponies-go-new-game-at-hubworld.html

* PonyTouhou
https://www.mediafire.com/file/mfnqurjuy87aaqg/PonyTouhou.zip
http://www.equestriagaming.net/2012/08/review-bullet-hell-ponies-my-little_3.html

* Starcraft vs MLP
https://www.equestriadaily.com/2011/06/starctaft-vs-mlp-bullethell-game.html

* Flutternight 
https://www.equestriadaily.com/2015/10/flutternight-halloween-game.html

* Pink-Tac-Toe
https://www.equestriadaily.com/2015/06/game-pink-tac-toe.html

* Sweet Apple Cider
https://www.equestriadaily.com/2013/02/game-sweet-apple-cider.html

* Buck those apples
https://www.equestriadaily.com/2014/09/game-buck-those-apples.html
https://www.newgrounds.com/portal/view/645858

* Cherry Sorter
http://www.equestriagaming.net/2012/05/cherry-sorter.html

* Coloring With Sweetie Belle
https://www.equestriadaily.com/2014/02/coloring-with-sweetie-belle.html

* Cadance Toss
http://www.equestriagaming.net/2012/11/cadance-toss.html
https://www.equestriadaily.com/2012/11/cadance-toss.html
http://www.equestriagaming.net/2013/01/cadence-throw-aka-best-game-ever.html

* Mysterious Muffin Mercenary Mare
https://www.equestriadaily.com/2012/07/game-mysterious-muffin-mercenary-mare.html 

* Twilight Sparkle - Pest Exterminator
https://www.equestriadaily.com/2012/07/game-twilight-sparkle-pest-exterminator.html

* Waiting is Magic
https://www.equestriadaily.com/2012/05/game-waiting-is-magic.html

* Rainbow dash attack
https://www.equestriadaily.com/2016/02/rd-day-classic-rainbow-dash-attack.html
https://www.equestriadaily.com/2011/07/rainbow-dash-attack.html

* Trosh The Movie: The Game
http://stabyourself.wikia.com/wiki/Trosh:_The_Movie:_The_Game

* Prismatic rings
https://www.equestriadaily.com/2014/10/game-prismatic-rings.html

* Twilight Sparkle vs Trixie
https://www.equestriadaily.com/2013/10/game-twilight-sparkle-vs-trixie.html

* The Reddit game
https://www.equestriadaily.com/2013/08/game-reddit-game.html

* Twilight's Epic Hill Ride
https://www.equestriadaily.com/2012/09/game-twilights-epic-hill-ride.html

* Equestria Corrupted
https://www.equestriadaily.com/2012/08/game-equestria-corrupted-release-trailer.html

* Trixie's Tomato Toss
https://www.equestriadaily.com/2012/06/trixies-tomato-toss.html

* Pinkie Jump
https://www.equestriadaily.com/2012/06/game-pinkie-jump.html
https://www.equestriadaily.com/2011/10/pinkie-jump-flash-game.html

* Canterlot Defender
https://www.equestriadaily.com/2012/05/game-canterlot-defender.html

* Equestria Daily: The Game
https://www.equestriadaily.com/2012/03/equestria-daily-game-hits-android.html

* My Little Pegasus: Kizuna DoPonyPachi
https://www.equestriadaily.com/2012/05/rainbow-dash-bullet-hell-game.html

* Catch the Scootaloos
https://www.equestriadaily.com/2011/12/game-catch-scootaloos.html

* Pinkie-Palooza
https://www.equestriadaily.com/2011/11/game-pinkie-palooza.html

* Twilight's Library Defense
https://www.equestriadaily.com/2011/11/twilights-library-defense.html

* The Fiends from Dream Valley
https://www.equestriadaily.com/2012/04/fiends-from-dream-valley-demo-launch.html
https://www.equestriadaily.com/2011/11/fiends-from-dream-vallery-update-8.html

* Luna vs. Fun
https://www.equestriadaily.com/2011/10/game-luna-vs-fun.html

* Ponywings
https://www.equestriadaily.com/2011/10/game-ponywings-scootaloo-on.html

* Super Filly Adventure
http://www.equestriagaming.net/2012/01/sopa-pipa-filly-adventure.html
http://www.equestriagaming.net/2012/02/review-super-filly-adventure.html
https://www.equestriadaily.com/2011/09/super-filly-adventure.html

* Runaway Pony
https://www.equestriadaily.com/2011/08/game-runaway-pony.html

* Spitfire vs the Shadowbolts
https://www.equestriadaily.com/2011/11/games-appleblooms-applebuck.html

* Appleblooms' Applebuck
https://3.bp.blogspot.com/-It45F_bJRKg/TrES_sk3cpI/AAAAAAAAQf4/AQC3F9L24bw/s320/Capture.PNG

* Pipsquek's Adventure
https://www.equestriadaily.com/2011/10/games-pipsqueaks-adventure.html

* Derpy Hooves sweet dream
http://creativewizzard.deviantart.com/art/Derpy-Hooves-sweet-dream-295338308
https://www.equestriadaily.com/2012/04/games-solar-empire-strikes-back-derpy.html

* Derpy's Story
https://www.equestriadaily.com/2012/08/game-derpys-story.html

* Twilight Sparkle's Revenge
http://www.equestriagaming.net/2014/02/demo-twalaght-sparkuhlz-revehnshe-2_39.html
https://www.equestriadaily.com/2012/08/game-twilight-sparkles-revenge-trailer.html
http://www.equestriagaming.net/2013/01/review-twalaght-sparkuhlz-revehngshe-11.html

* UFO Repel (with ponies!)
https://www.equestriadaily.com/2012/06/game-ufo-repel-with-ponies-pinkies.html

* Cloud Cannon
https://www.deviantart.com/cloneconstructor/art/Cloud-Cannon-309053298

* Thrack it Trixie
https://sparklepeep.deviantart.com/art/Thrack-It-Trixie-341442585

* Animals Juggling
https://fearlesspie.deviantart.com/art/Animal-Juggling-341565393

* Rainbow's Tail
https://sylrepony.deviantart.com/art/Rainbow-s-Tail-341082618

* Applejack's Awesome Apple Acquisition
https://futzi01.deviantart.com/art/Applejack-s-Awesome-Apple-Acquisition-340357135

* Pie!
https://www.equestriadaily.com/2012/11/game-pie_16.html

* Luna's Pumpkin Launcher
https://www.equestriadaily.com/2012/10/game-lunas-pumpkin-launcher.html

* Rainbow Dash's cloud dodging fun
http://www.pentastudios.net/index.php?page=game&id=4&mode=play

* Muffin Catcher
https://fearlesspie.deviantart.com/art/Muffin-Catcher-344773158?q=gallery%3Afearlesspie&qo=0

* Rainbow Dash Attack Prizm
https://www.equestriadaily.com/2012/07/game-trailerspreviews-rainbow-dash.html

* Rainbow Dash Mini Game
https://urimas.deviantart.com/art/Rainbow-Dash-Mini-Game-DONE-V3-201264368

* The way home
https://www.equestriadaily.com/2011/03/game-way-home.html
http://www.yoyogames.com/games/168448-mlp-fim-the-way-home

* 200 bit fish
http://www.equestriagaming.net/2017/07/200-bit-fish.html

* Hop, Skip & Jump
http://www.equestriagaming.net/2014/08/mini-game-hop-skip-jump_37.html

* Servant of Discord
http://www.equestriagaming.net/2013/04/servant-of-discord-updatescreenshotsrec.html

* The Amazing Jumping Adventure
http://www.equestriagaming.net/2014/02/the-amazing-jumping-adventure_63.html

* Apples
http://www.equestriagaming.net/2013/03/gameapples.html

* Pinkie or Not Pinkie
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews_18.html

* Iron Pony: Table Tennis
https://www.equestriadaily.com/2013/03/game-iron-pony-table-tennis.html

* Iron Pony Challenge
https://www.equestriadaily.com/2011/09/game-iron-pony-challenge.html

* Iridescent Pony Warrior
http://www.equestriagaming.net/2013/05/iridescent-pony-warrior-beta.html


<a name="quests"></a>
## Quests

* Octavia in the Underworld's Cello
https://www.equestriadaily.com/2014/11/demo-releases-for-octavia-and.html
https://herooftime1000.deviantart.com/art/The-Final-Door-743148674

* My Little Investigations
https://www.equestriadaily.com/2017/06/my-little-investigations-returns-for.html

* Banned From EquestriaDaily
http://mlpfanart.wikia.com/wiki/Banned_From_Equestria_(Daily)

* Moonstuck
https://www.equestriadaily.com/2013/03/game-moonstuck.html

* Phoenix Wright - Turnabout Adoption (mod)
https://www.equestriadaily.com/2013/07/game-phoenix-wright-turnabout-adoption.html

* Doli Incapax: Horse Lawyer
http://www.equestriagaming.net/2014/03/doli-incapax-horse-lawyer_49.html
https://web.archive.org/web/20140408163109/http://aceattorney.sparklin.org/jeu.php?id_proces=57999


<a name="rpgs"></a>
## RPGs/JRPGs

* Story of the Blanks
http://www.equestriagaming.net/2011/07/story-of-blanks.html
https://www.equestriadaily.com/2011/07/nes-style-pony-rpg-story-of-blanks.html

* Story of the Blanks HD
http://www.equestriagaming.net/2014/05/demo-footage-of-story-of-blanks-hd_32.htmls

* My Little Fucking Pony
https://www.youtube.com/watch?v=XGU6cnDuLHg

* War of Harmony IV
http://www.mediafire.com/?qcxojimcg7b0s
http://www.equestriagaming.net/2014/12/war-of-harmony-platformer-beta_45.html
http://mlpfanart.wikia.com/wiki/War_of_Harmony_IV

* My Little Pony: The Bonds of Friendship
http://mlpfanart.wikia.com/wiki/My_Little_Pony:_The_Bonds_Of_Friendship

* Sonic X: Adventures in Equestria
http://mlpfanart.wikia.com/wiki/Sonic_X:_Adventures_in_Equestria

* Project Harmony
http://www.equestriagaming.net/2012/10/projectharmony-teaser.html

* Meloetta: Melody of Discord
http://www.equestriagaming.net/2013/02/meloetta-melody-of-discord.html

* Rise of the Ponies
http://www.equestriagaming.net/2013/08/trailer-rise-of-ponies.html

* Pokepon
http://www.equestriagaming.net/2014/10/pokepon-early-alpha-release_30.html

* My Little Pokemon
http://www.equestriagaming.net/2011/05/my-little-pokemon-game-trailer.html

* Ponymon Dawn/Dusk
http://www.equestriagaming.net/2012/09/ponymon-dawndusk-03-alpha-release.html

* MLP RPG: The Elements of Harmony
http://www.equestriagaming.net/2012/12/my-little-pony-rpg-elements-of-harmony.html
https://steamcommunity.com/groups/MLPRPGElementsofHarmony

* Fnagame
http://www.equestriagaming.net/2013/08/fnagame-review.html

* Gates of Tartarus 0.3.1
http://www.equestriagaming.net/2013/08/gates-of-tartarus-031.html

* Twilight Sparkle Adventures
http://www.equestriagaming.net/2012/02/twilight-sparkle-adventures-beta.html

* Twilight Sparkle RPG
http://www.equestriagaming.net/2012/09/twilight-sparkle-rpg-working-title.html

* Doomsday Ascending
http://www.equestriagaming.net/2012/10/review-doomsday-ascending-part-1.html

* Super Lesbian Horse Game
https://www.equestriadaily.com/2013/12/super-lesbian-horse-rpg-release.html

* Day Dreaming Derpy
https://www.equestriadaily.com/2018/01/day-dreaming-derpy-officially-complete.html

* Filly Fantasy 6
https://www.equestriadaily.com/2017/11/filly-fantasy-20-released.html

* Adventure of the Lunarbolts RPG
https://www.equestriadaily.com/2017/12/adventure-of-lunarbolts-finally-gets.html

* Battle Gem Ponies
https://www.equestriadaily.com/2018/06/battle-gem-ponies-gets-another-facelift.html

* My Little Pony - Budding Friendships
https://www.equestriadaily.com/2012/02/game-my-little-pony-budding-friendships.html

* Dawn of new Equestria
https://www.equestriadaily.com/2016/10/a-new-pony-rpg-is-in-development.html

* Pony Chronicles 2
https://www.equestriadaily.com/2013/01/game-trailers-pony-chronicles-2.html

* My Little Dashie - the game
https://www.equestriadaily.com/2012/02/my-little-dashie-game.html

* Between Dusk and Dawn
https://www.equestriadaily.com/2011/12/game-between-dusk-and-dawn.html
http://www.equestriagaming.net/2012/01/between-dusk-and-dawn-demo.html

* BFS
http://www.equestriagaming.net/2014/04/review-bfs_19.html

* Curse of the Lost Kingdom / My Little Pony X Jaws
http://www.equestriagaming.net/2012/12/my-little-pony-x-jaws.html
http://thebeansehreport.tumblr.com/
http://www.equestriagaming.net/2012/07/mlp-fim-lost-kingdom_31.html
http://www.equestriagaming.net/2013/11/curse-of-lost-kingdom-old-demo_27.html

* Blue Touch Cookie Caves
http://www.equestriagaming.net/2013/10/blue-cookie-touch-caves-rpg.html

* Fallout Equestria RPG
http://www.equestriagaming.net/2013/05/fallout-equestria-rpg-trailer.html

* A Chaotic Crisis
http://www.equestriagaming.net/2013/01/a-chaotic-crisis.html

* MLP Flash RPG
http://www.equestriagaming.net/2011/05/mlpfim-flash-sandbox-game-video-update.html
http://www.equestriagaming.net/2011/05/mlp-flash-rpg.html

* Luna's Quest
http://www.equestriagaming.net/2012/10/lunas-quest-help-needed.html


<a name="multiplayer"></a>
## Multiplayer games

* Battle Royal: Forward to the Past
http://www.equestriagaming.net/2014/01/battle-royale-forward-to-past_77.html

* Equestria Chronicles: Online
http://www.equestriagaming.net/2011/04/equestria-chronicles-online-contest.html

* Pinkie's Hyperactive Chat
http://www.equestriagaming.net/2012/07/pinkies-hyperactive-chat.html

* Ponyplace
http://www.equestriagaming.net/2012/11/ponyplace-updates.html

* Friendly Magic Duel
http://www.equestriagaming.net/2013/04/my-little-pony-friendly-magic-duel.html
http://browse.deviantart.com/art/My-Little-Pony-Friendly-Magic-Duel-366019177

* Legends of Equestria
https://www.equestriadaily.com/2018/03/legends-of-equestria-releases-winter.html

* Manequest/Pony Club
https://www.equestriadaily.com/2018/05/pony-club-releases-update-151-double.html
https://www.equestriadaily.com/2017/12/new-game-project-underway-manequest.html

* DOG & PONY GAME
https://www.equestriadaily.com/2018/04/dog-pony-game-is-officially-released.html

* PonyAge: Chronicles
https://www.equestriadaily.com/2018/07/ponyage-chronicles-pony-mmorpg-update.html

* Sunrise of Axoria (MOBA)
https://www.equestriadaily.com/2017/07/pony-moba-sunrise-of-axoria-enters.html

* PonyKart Racing
https://www.equestriadaily.com/2016/06/ponykart-inspired-pony-kart-racer-in.html

* Pony Town
https://www.equestriadaily.com/2016/08/be-pony-in-pony-town.html

* FanatSors' game
https://www.equestriadaily.com/2016/07/public-test-on-fanatsors-online-pony.html

* MLP RPG Merry Christmas
https://www.equestriadaily.com/2012/12/mlp-rpg-merry-christmas-video.html

* MLP Online
https://www.equestriadaily.com/2012/11/mlp-online-single-player-episode-1.html

* Pegadrome
https://www.equestriadaily.com/2012/06/game-pegadrome.html

* Equestria Online: Leaps and Bounds
https://www.equestriadaily.com/2012/01/equestria-online-leaps-and-bounds.html
http://www.equestriagaming.net/2012/03/update-on-equestria-onlinelegends-of.html

* Crazy Pony 1.2
http://www.equestriagaming.net/2013/11/crazy-pony-12_4.html


<a name="tabletop"></a>
## Tabletop games

* Randleshackle
http://www.equestriagaming.net/2013/07/board-game-randleshackle-playtesting.html

* Ponyhammer
http://www.equestriagaming.net/2012/12/ponyhammer-first-draft.html

* Heroes of Equestria
https://rpggeek.com/rpg/24561/heroes-equestria

* My Little Pony: Tails of Equestria Table Top RPG
https://www.equestriadaily.com/2018/07/tails-of-equestria-judge-not-by-cover.html

* Parasprite Panic!
https://www.equestriadaily.com/2013/02/parasprite-panic.html

* Planeshift: Equestria
https://www.equestriadaily.com/2018/03/new-homebrew-ponified-d-5th-edition.html

* Roan table top pony game
https://www.equestriadaily.com/2017/11/roan-table-top-pony-game-discount-event.html

* Fallout Equestria Pen and Paper Game
https://www.equestriadaily.com/2017/04/fallout-equestria-pen-and-paper-game.html

* Ponyfinder - Dawn of the Fifth Age
https://www.equestriadaily.com/2016/08/ponyvinder-dawn-of-hte-fifth-age-final.html

* Ponyfinder - Griffons of Everglow
https://www.equestriadaily.com/2014/06/ponyfinder-griffons-of-everglow.html

* Twilight Sparkle's Secret Shipfic Folder
https://www.equestriadaily.com/2014/10/unleash-bats-twilight-sparkle-secret.html

* Roleplaying is Magic
https://www.equestriadaily.com/2014/07/roleplaying-is-magic-season-four-edition.html

* Courage is Magic
https://www.equestriadaily.com/2014/12/courage-is-magic-118-releases-full.html

* Buckshot
https://www.equestriadaily.com/2016/02/new-pony-cap-flicking-game-buckshot.html

* Sun and Moon
https://www.equestriadaily.com/2012/09/sun-and-moon-pony-strategy-game.html

* Friendship is Magic RPG
https://www.equestriadaily.com/2013/03/game-trailers-pony-civil-war-friendship.html

* MLP RPG (Fighting the Unknown)
http://www.equestriagaming.net/2013/03/mlp-rpgfighting-unknown-update.html
http://www.equestriagaming.net/2013/12/mlp-rpg-fighting-unknown-update_23.html

* Galloping Stars
https://www.equestriadaily.com/2018/06/galloping-stars-rpg-kickstarter-hits.html

* Luna's Falling Stars
https://www.equestriadaily.com/2014/05/game-lunas-falling-stars.html

* Ponyville Mystery
https://www.equestriadaily.com/2014/02/ponyville-mystery-lovecraft-style-board.html

* Shards of Equestria
https://www.equestriadaily.com/2012/07/shards-of-equestria-pony-magic.html

* My Little Uno
https://www.equestriadaily.com/2011/09/my-little-uno.html

* Murder Mystery
https://www.equestriadaily.com/2011/08/friendship-is-magic-murder-mystery-game.html

* Invasion of the pony snatchers
https://www.equestriadaily.com/2011/10/pony-game-compilation.html
https://docs.google.com/a/equestriadaily.com/document/d/1vWQnZVTHUVkCT63eapKRpiwQkT2PKcKQ3DW51GUUiFw/edit?hl=en_US&pli=1

* Pony Tales: Aspirations of Harmony
http://mlpfanart.wikia.com/wiki/Pony_Tales:_Aspirations_of_Harmony


<a name="strategies"></a>
## Strategies

* Super Pony Wars
http://project-spw.tumblr.com/post/69079531954/project-super-pony-wars-12-05-13-release

* MEW (Mock Equestrian Wars)
http://www.equestriagaming.net/2012/07/mew-game-elements-demonstration.html

* Super Pony War
http://www.equestriagaming.net/2014/07/demo-super-pony-wars_68.html

* Pony Emblem
http://www.equestriagaming.net/2012/03/pony-emblem.html

* Shattered Kingdom
http://www.equestriagaming.net/2014/12/shattered-kingdom_12.html

* Герои Эквестрии
http://heroesofequestria.org/doku.php

* My Little Miner: Disturbance is Magic
https://www.equestriadaily.com/2013/12/my-little-minder-disturbance-is-magic.html

* Nightfall
https://www.equestriadaily.com/2015/03/nightfall-gets-huge-update.html

* Little Pony Big War 
https://www.equestriadaily.com/2013/05/game-little-pony-big-war.html

* MLP TD
https://www.equestriadaily.com/2012/07/game-trailerspreviews-rainbow-dash.html

* Princess Civilization
http://www.equestriagaming.net/2013/06/review-princess-civilization.html

* Canterlot Siege 4
https://www.equestriadaily.com/2015/09/canterlot-siege-4-power-ponies-edition.html

* Pony Civil War
https://www.equestriadaily.com/2013/03/game-trailers-pony-civil-war-friendship.html

* CLOP
http://www.equestriagaming.net/2014/01/everybody-loves-clop_96.html

* Everwinter
http://www.equestriagaming.net/2013/01/game-everwinter.html

* Age of Canterlot
http://www.equestriagaming.net/2014/01/age-of-canterlot_69.html


<a name="action"></a>
## 3d / Action games

* Derpy's Gr8 Adventure
http://www.equestriagaming.net/2017/03/derpys-gr8-avdenture-trilogy.html

* Twilight: Escape from Stalliongrad (Wolfenstein 3D mod)
http://mlpfanart.wikia.com/wiki/Twilight:_Escape_from_Stalliongrad

* 20% Faster (Abandoned?)
http://www.equestriagaming.net/2013/12/20-faster_11.html
https://www.equestriadaily.com/2013/12/game-demonstration-korean-rainbow-dash.html

* Castle Pinkiestein (Abandoned)
http://www.equestriagaming.net/2014/01/castle-pinkiestein_25.html

* 3D My Little Pony Indie Game
http://www.equestriagaming.net/2011/05/3d-my-little-pony-indie-game-demo-update.html

* My Little Pony 3D Open Source
http://www.equestriagaming.net/2011/05/mlpfim-flash-sandbox-game-video-update.html

* Immolation (Abandoned)
http://www.equestriagaming.net/2012/03/rather-large-immolation-update.html

* Shattered Limits
http://www.equestriagaming.net/2013/05/shattered-limits.html
http://shatteredlimitsgames.tumblr.com/download

* Ashes of Equestria
https://www.equestriadaily.com/2017/03/fallout-equestria-gets-new-name-and-demo.html

* RariDOOM (mod)
http://www.equestriagaming.net/2013/01/raridoom-beta.html

* Horse Game
https://www.equestriadaily.com/2017/02/horse-game-still-in-development-seeking.html

* Daring Do Origins
https://www.equestriadaily.com/2017/06/daring-do-origins-new-fan-game-in.html

* Friendship is Fragging (Doom mod)
http://www.equestriagaming.net/2012/05/friendship-is-fragging-video-update.html

* Ambient.White
https://www.equestriadaily.com/2017/12/ambient-releases-update-about-current.html

* Gardens of Equestria: This Coming Storm (Fallout: New Vegas mod)
https://www.equestriadaily.com/2017/02/gardens-of-equestria-this-coming-storm.html

* Sulphur Nimbus: Hel's Elixir (kickstarter)
https://www.equestriadaily.com/2016/10/sulphur-nimbus-kickstarter-ending-today.html

* Equestria Tales: Elements of Chaos (Abandoned?)
http://www.equestriagaming.net/2011/09/equestria-tales.html
https://www.equestriadaily.com/2011/11/equestria-tales-elements-of-chaos-video.html

* No Guns in Canterlot (Abandoned)
http://www.equestriagaming.net/2013/12/no-guns-in-canterlot_3.html

* Medieval Equestria (Abandoned?)
https://www.equestriadaily.com/2014/07/game-medieval-equestria-project.html

* Ponycraft - Tainted Skies (WC3 mod)
https://www.equestriadaily.com/2013/05/game-ponycraft-tainted-skies.html

* Breach of Harmony (Abandoned)
https://www.equestriadaily.com/2013/12/game-breach-of-harmony-pre-alpha-preview.html

* Rotor -> Sulphur Nimbus
http://www.equestriagaming.net/2013/03/trailer-rotor.html
https://www.equestriadaily.com/2013/03/game-trailers-rotor-harmony-pcg-pinkie.html
https://www.equestriadaily.com/2013/01/games-trailers-mlp-rpg-and-random.html

* H.O.A.R.S.E.
http://www.equestriagaming.net/2015/02/hoarse_32.html
http://www.playground.ru/flash/h_o_a_r_s_e_trehmernaya_malenkaya_poni-15836/


<a name="mobile"></a>
## Mobile games

* Pinkamena
https://www.equestriadaily.com/2016/06/new-android-game-pinkamena.html

* Unicorn Training
https://www.equestriadaily.com/2015/06/adventures-in-equica-unicorn-training.html

* My Little Pony: Pocket Ponies (OFFICIAL)
https://www.equestriadaily.com/2018/07/my-little-pony-pocket-ponies-game-to-be.html

* Rainbow Runners:
https://www.equestriadaily.com/2017/06/new-pony-game-on-itunes-store-rainbow.html

* Lights, Cameras, Ponies!
https://www.equestriadaily.com/2016/09/new-pony-game-and-message-stickers-from.html

* Pony Creator App
https://www.equestriadaily.com/2015/11/a-new-pony-creator-app-appears-for.html

* Two muffins
https://www.equestriadaily.com/2015/02/game-two-muffins.html

* Questria
https://www.equestriadaily.com/2012/09/game-questria-trailer-and-alpha-gameplay.html

* Rainbow Cloud Attack Game
https://www.equestriadaily.com/2011/10/rainbow-cloud-attack-game-for-android.html

* Pony Rush
https://play.google.com/store/apps/details?id=com.pony.rush.android

* Zap Apple Buckin'
http://www.equestriagaming.net/2013/12/zap-apple-buckin_6.html

* Gameloft MLP game (official)
https://www.equestriadaily.com/2018/06/gameloft-mlp-app-adds-rainbow-blaze-and.html


<a name="music"></a>
## Rhythm/music games

* Equestria Jukebox
http://www.equestriagaming.net/2014/01/equestria-jukebox_37.html

* Pony Piano
https://www.equestriadaily.com/2011/06/pony-piano.html

* Pony Soundboard
https://www.equestriadaily.com/2011/06/massive-updates-to-soundboard.html

* Luna and the Glass Harp
https://www.equestriadaily.com/2011/09/luna-and-glass-harp-music-game.html

* Patapony
https://www.equestriadaily.com/2012/06/game-patapony.html

* Pony Rythm Game
https://www.equestriadaily.com/2017/12/pony-rythm-game-back-in-development.html

* My Little Karaoke
https://www.equestriadaily.com/2016/04/my-little-karaoke-new-update-fixing.html

* Trotmania
https://www.equestriadaily.com/2018/02/trotmania-v-is-on-its-way-reniassance.html

* Ponytone
https://www.equestriadaily.com/2017/09/new-pony-karaoke-online-game-ponytone.html

* Notes From Magic
https://www.equestriadaily.com/2015/12/new-fanmade-pony-rhythm-game-in.html

* Faithful Farmer
https://www.equestriadaily.com/2012/07/game-faithful-farmer.html

* Sweetie Bot's Pixel Trip
https://www.equestriadaily.com/2013/02/game-sweetie-bots-pixel-trip.html

* Super Speed Cider Squeezy 6000
https://www.equestriadaily.com/2012/06/game-super-speedy-cider-6000.html

* Lyra and Bonbon Music Game
https://www.equestriadaily.com/2011/10/lyra-and-bonbon-music-game.html

* Octavia Cello
https://www.equestriadaily.com/2011/08/octavia-cello-game.html

* My Little Pimp It Up
http://www.equestriagaming.net/2012/09/my-little-pump-it-up-project.html

* Project Magic
http://www.equestriagaming.net/2013/10/project-magic.html


<a name="point"></a>
## Point-and-click games

* Mine Escape
https://www.equestriadaily.com/2012/07/mine-escape-game.html

* Adventure In Ponyville 0.1
http://www.equestriagaming.net/2013/02/adventure-in-ponyville-01.html
https://igrezadecu.com/my_little_pony/adventures_in_ponyville/

* Please, Make me smile!
http://www.equestriagaming.net/2012/06/please-make-me-smile.html
http://luna.hamcha.net/SMILE_EN.exe

* Spike's Time Off 
https://www.equestriadaily.com/2018/07/spikes-time-off-sizzle-trailer.html

* Spike's Day Out 
https://www.equestriadaily.com/2016/05/game-spikes-day-out.html

* Spike's Day Off
https://www.equestriadaily.com/2016/02/short-game-spikes-day-off.html

* Where's Derpy?
https://www.equestriadaily.com/2014/03/game-wheress-derpy-3.html


<a name="fightings"></a>
## Fightings

* Awesome Pony Fighting Game
http://www.equestriagaming.net/2013/11/awesome-pony-fighting-game_4.html

* Them's Fightin' Herds / Fighting is Magic
https://www.equestriadaily.com/2016/12/fighting-is-magic-tribute-edition-test.html
https://www.equestriadaily.com/2017/02/awful-week-for-fighting-is-magic.html

* Friendship is Epic
https://www.equestriadaily.com/2017/02/1-hour-of-friendship-is-epic-gameplay.html

* Cutie Mark Crusade: A Dash of Adventure
http://mlpfanart.wikia.com/wiki/My_Little_Pony_Fan_Labor_Wiki:Workshop/Cutie_Mark_Crusade:_A_Dash_Of_Adventure

* Super Smash Ponies
http://www.equestriagaming.net/2012/10/super-smash-ponies.html
https://www.equestriadaily.com/2014/11/super-smash-ponies-rainbow-dash-theme.html


<a name="horror"></a>
## Horror games

* Never Heard from Ever Again
http://www.equestriagaming.net/2013/10/released-never-heard-from-ever-again_29.html

* Cupcakes 4: New ZAT Era
http://mlpfanart.wikia.com/wiki/Cupcakes_4:_New_ZAT_Era

* Equestfold: Hospital Anoreham
https://www.equestriadaily.com/2018/06/demo-version-of-equestfold-hospital.html

* Five Nights at Pinkie's
https://www.equestriadaily.com/2017/07/five-nights-at-pinkies-game-4-releases.html

* Fall of Anterfold: Outland
https://www.equestriadaily.com/2018/05/fall-of-anterfold-outland-full-version.html

* Fall of Anterfold 2: Chronicles
https://www.equestriadaily.com/2018/01/fall-of-anterfold-chronicles-2-is-now.html

* Fall of Anterfold: Chronicles
https://www.equestriadaily.com/2017/10/fall-of-anterfold-chronicles-is-now.html

* Silent Ponyville
https://www.equestriadaily.com/2017/10/silent-ponyville-rpg-maker-game.html
https://www.equestriadaily.com/2011/09/silent-ponyville-game.html

* Derp till Dawn
https://www.equestriadaily.com/2013/04/derp-till-dawn.html

* The Town That Feared Nightfall
https://www.rpgmakercentral.com/topic/39580-the-town-that-feared-nightfall-demo/

* My Silent Ville
http://www.equestriagaming.net/2017/10/my-silent-ville-whoa-someone-actually.html

* Luna Game 3D
http://www.equestriagaming.net/2017/08/luna-game-3d.html

* Luna Game End
http://www.equestriagaming.net/2012/08/luna-game-end-creepypasta.html

* Muffins.exe (Creepypasta)
http://www.equestriagaming.net/2013/10/review-muffinsexe-creepypasta_31.html

* Twilight escape
https://www.equestriadaily.com/2015/03/game-twilight-escape-trailer.html

* Pony Space
https://www.equestriadaily.com/2011/11/game-pony-space.html

* A World You Should Not Know
http://www.mylittlegamedev.com/forums/showthread.php?tid=864

* Island of Falling Stars
http://www.equestriagaming.net/2014/01/review-island-of-falling-stars_17.html

* The Unknown
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews.html

* My Little Foundation: Containment is Magic
http://ru.scpcontainmentbreach.wikia.com/wiki/My_Little_Foundation:_Containment_is_Magic

* D'LIRIUM
https://www.equestriadaily.com/2017/08/creepy-pony-game-dlirium-hitting-steam.html


<a name="simulators"></a>
## Simulators

* Pegasus Flight Simulator
http://mlppegasusflightsim.webs.com/downloads

* Luna's Letter
http://www.equestriagaming.net/2014/01/lunas-letter_99.html

* Equestria Racing
http://www.equestriagaming.net/2014/04/equestria-racing_95.html

* Dashcon Simulator 2014
http://www.equestriagaming.net/2014/07/dashcon-simulator-2014_18.html
http://www.kongregate.com/games/dashcon2014/dashcon-simulator-2014

* Pony Tales Adventures
https://www.equestriadaily.com/2018/02/new-mlp-dating-simvisual-novel-in-works.html

* My Little Sweetheart
https://www.equestriadaily.com/2018/01/my-little-sweetheart-dating-sim-gets.html

* Ponk Dating Sim
https://www.equestriadaily.com/2017/09/ponk-dating-sim-in-works.html

* Pony Dating Sim
https://www.equestriadaily.com/2017/05/pony-dating-sim-gets-infusion-of.html

* Heartwarmer's simulators
http://www.equestriagaming.net/2017/04/playing-simulator-4-sunset-shimmer.html

* The Difference Between Us
https://www.equestriadaily.com/2017/03/the-difference-between-us-fluttershy-x.html

* PixelShy
https://www.equestriadaily.com/2016/08/game-pixelshy-promo-released.html

* PonyVRville
https://www.equestriadaily.com/2015/12/ponyvrville-updates-better-graphics.html

* Twilight Vs. Walking
https://www.equestriadaily.com/2012/07/game-qwop-with-ponies-twilight-vs.html

* The Ponies (The Sims)
https://www.equestriadaily.com/2018/01/new-ponies-sims-style-gaming-project.html

* Rainbow Dash flying simulator
https://www.equestriadaily.com/2016/05/rainbow-dash-flying-simulator.html

* EQD Clicker game
https://www.equestriadaily.com/2015/05/eqd-clicker-game-experience-life-of.html

* Pony Clicker
https://www.equestriadaily.com/2015/05/time-for-addiction-pony-clicker-arrives.html

* Too Many Ponies:
https://www.equestriadaily.com/2014/05/game-too-many-ponies.html

* PinkieShenanigans 2
https://www.equestriadaily.com/2014/01/game-pinkieshenanigans-2.html

* Questria: Princess Destiny
https://www.indiegogo.com/projects/questria-princess-destiny#/
http://www.equestriagaming.net/2013/08/questria-princess-destiny-on-indiegogo.html

* Explore Ponyville
https://www.equestriadaily.com/2013/02/explore-ponyville.html

* Derpy Buddy
https://www.equestriadaily.com/2012/03/derpy-buddy.html

* Pony Amnesia: Love & Mysteries
https://www.equestriadaily.com/2013/11/game-pony-amnesia-love-mysteries-demo.html

* Tom the Rock Simulator
http://www.equestriagaming.net/2014/03/tom-rock-simulator_31.html

* Luna Wander
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews.html

* Joy Pony
https://www.youtube.com/watch?v=2_zD3KrN7vc


<a name="puzzles"></a>
## Puzzles

* MLP Matching Is Magic
http://www.equestriagaming.net/2013/05/game-mlp-matching-is-magic.html

* NIA: Path of Light -> Path of Planes
https://www.equestriadaily.com/2017/07/new-nia-path-of-light-teaser-released.html

* Master of Parasprites
https://www.equestriadaily.com/2015/05/master-of-parasprites-game-heads-into.html

* Pony Portal
http://www.equestriagaming.net/2012/05/pony-portal-advance.html

* Luna's Stars
https://www.equestriadaily.com/2014/08/game-lunas-stars.html

* Celestia's Cake Golf Adventure in Space 
https://www.equestriadaily.com/2014/01/game-celestias-cake-golf-adventure-in.html

* Twilight Sparkle's Magical Mysteries (official)
https://www.equestriadaily.com/2012/11/game-twilight-sparkles-magical-mysteries.html

* My Litle Mahjong
https://www.equestriadaily.com/2012/11/new-game-almost-on-hub-my-little-majong.html

* Pinkie Pie's Cupcake Maker (official)
https://www.equestriadaily.com/2012/10/pinkie-pies-cupcake-maker.html

* Pony Jigsaw Puzzles
https://www.equestriadaily.com/2012/02/pony-jigsaw-puzzles.html

* Trinelight
https://www.equestriadaily.com/2011/09/trilight-demo-test.html
https://www.equestriadaily.com/2011/07/flash-game-trinelight-beta.html


<a name="visual"></a>
## Visual Novels

* Moon Rising Simulator
https://yadi.sk/d/HAMctt2uyKHvE

* My Little Pony: Ace Attorney
http://mlpfanart.wikia.com/wiki/My_Little_Pony:_Ace_Attorney_-_School_Yard_Trials
http://mlpfanart.wikia.com/wiki/My_Little_Pony_-_Ace_Attorney:_Trials_in_Equestria

* Super Ethical Shipping Climax Demo
http://www.equestriagaming.net/2014/01/super-ethical-shipping-climax-demo_70.html

* A Foal's Errand
http://www.equestriagaming.net/2013/07/a-foals-errand.html

* Pony Visual Novel
http://www.equestriagaming.net/2011/04/pony-visual-novel.html
http://www.ponychan.net/chan/collab/res/15.html

* Quest of Memories
https://sites.google.com/site/mlpquestofmemories/

* Starswirl Academy
https://www.equestriadaily.com/2016/05/starswirl-academy-update-faith.html

* ADVenture Time: Искорка ревности
https://everypony.ru/vizualnaya-novella-adventure-time-iskorka-revnosti

* Welcome to Ponyville
https://www.equestriadaily.com/2012/09/welcome-to-ponyville-act-1-released.html

* Epic Portal Time
https://www.equestriadaily.com/2011/11/flash-game-epic-portal-time.html

* Vinyl's Silent Wish
https://www.equestriadaily.com/2011/10/vinyls-silent-wish.html

* Lights in the Sky
http://www.equestriagaming.net/2013/06/lights-in-sky-brony-fan-fair-visual.html
http://bronyfanfair.com/?page_id=873&preview=true

* Persona: Pony
http://www.equestriagaming.net/2012/02/personapony-demo.html
https://www.equestriadaily.com/2012/02/game-persona-pony.html


<a name="text"></a>
## Text games

* Star Swirl's Parser Based Adventure
http://www.wikifortio.com/622195/StarSwirlSetup.exe

* Death By Discord
http://www.textadventures.co.uk/review/655/

* Welcome to Ponyville
http://www.textadventures.co.uk/review/425/

* Primaeval Threat
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews_18.html

* Brony Hero of Equestria
https://www.equestriadaily.com/2015/12/brony-hero-of-equestria-new-cyoa-game.html

* Doctor Whooves - Hidden Signal 
https://www.equestriadaily.com/2015/07/game-doctor-whooves-hidden-signal.html

* Wounded: An MLP Text Adventure
http://www.java-gaming.org/topics/wounded-an-mlp-text-adventure/31437/view.html

* Spes Fractus
http://www.equestriagaming.net/2013/01/text-adventures.html
http://www.equestriagaming.net/2013/01/spes-fractus.html

* The Mansion
https://www.equestriadaily.com/2013/12/choose-your-own-adventure-picture-game.html

* The Manor
http://www.equestriagaming.net/2017/02/gem-hunting-2-malicious-mansions-silly.html

* The Purloined Pony
https://www.equestriadaily.com/2011/05/storygame-purloined-pony.html

* Derpy & The hunting of the Super Tasty Muffin
http://www.equestriagaming.net/2014/07/review-derpy-hunting-of-super-tasty_8.html

* Saddle Arabian Nights
http://www.equestriagaming.net/2014/07/pen-and-paper-rpg-mlp-saddle-arabian_11.html

* My Little Time Killer
http://textadventures.co.uk/games/view/drpsfkrj5ewjzxgiut2keq/my-little-time-killer

* Welcome to Dream Valley
http://textadventures.co.uk/games/view/s2vjhbzimualm71cgzlhhw/welcome-to-dream-valley-abandoned


<a name="generators"></a>
## Generators

* PoniPoni Visual Novel Creator
http://www.equestriagaming.net/2013/12/poniponi-visual-novel-creator_85.html

* Pony Couple Generator
https://www.equestriadaily.com/2018/06/ship-some-pones-pony-couple-generator.html

* Pony Comic Generator
https://www.equestriadaily.com/2017/11/pony-comic-generator-updates-60-new.html

* Dress up:
https://www.equestriadaily.com/2017/07/animated-dress-up-pony-creator-on.html

* Pixel pony creator
https://www.equestriadaily.com/2017/06/pixel-pony-creator-now-up.html

* 3D Pony Creator
https://www.equestriadaily.com/2014/03/3d-pony-creator.html

* Pony Puzzles
https://sylrepony.deviantart.com/#/d4y9517

* The Moon
http://www.equestriagaming.net/2013/06/my-little-game-dev-marathon-reviews_18.html


<a name="rom"></a>
## ROM games

* Daring Do NES Style
http://www.equestriagaming.net/2012/09/daring-do-nes-trailer-demo.html
https://www.equestriadaily.com/2012/08/game-daring-do-nes-style-trailer.html

* https://www.equestriadaily.com/2016/12/a-look-at-pony-games-being-sold-on.html


<a name="other"></a>
## Other games

* PonyRL
http://www.equestriagaming.net/2014/01/ponyrl-progresses-smoothly_59.html

* Anquestria
http://www.equestriagaming.net/2011/07/anquestria.html

* Equestria World - The Videogame
https://www.equestriadaily.com/2014/06/gamepixel-art-equestria-world-videogame.html

* Pony Emblem - Tactics and Ponies!
https://www.equestriadaily.com/2012/03/game-pony-emblem-tactics-and-ponies.html

* Pony MUSH
https://www.equestriadaily.com/2011/04/pony-mush.html

* Pony Tactics: Salvation -> Spectrum Tactics
https://www.equestriadaily.com/2012/12/game-pony-tactics-salvation-trailer.html
http://www.equestriagaming.net/2013/03/pony-tactics-to-become-spectrum-tactics.html

* Youtube game:
https://www.equestriadaily.com/2016/01/heroes-of-equestria-youtube-game.html


<a name="bundles"></a>
## Demo bundles

* https://www.equestriadaily.com/2017/10/russian-indie-pony-game-reel-from-toki.html

* https://www.equestriadaily.com/2014/01/my-little-game-jam-25-developers.html

* http://www.mylittlegamedev.com/showthread.php?tid=855&pid=5027

* MLP Starter Pack
https://dl.dropboxusercontent.com/s/6fufbefg2uwbd6y/MLP%20starting%20pack.exe?token_hash=AAHgm3tsGopAuvqIs3ap2LeGJIbp9DEPa1d_sW5WfRcXPA&dl=1

